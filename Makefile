де.PHONY: docs clean

COMMAND = docker-compose exec -T djangoapp /bin/bash -c

all: build test

build:
	docker-compose build

run:
	docker-compose up

migrate:
	$(COMMAND) "cd hello && python3 ./manage.py migrate --database=database2"

collectstatic:
	docker-compose run --rm djangoapp hello/manage.py collectstatic --no-input

check: checksafety checkstyle

test:
	$(COMMAND) "python3 -m venv env && pip install tox && tox -e test"

checksafety:
	$(COMMAND) "python3 -m venv env && pip install safety && tox -e safetycheck"

checkstyle:
	$(COMMAND) "python3 -m venv env && pip install prospector && tox -e checkstyle"

coverage:
	$(COMMAND) "pip install tox && tox -e coverage"

clean:
	rm -rf build
	rm -rf hello.egg-info
	rm -rf dist
	rm -rf htmlcov
	rm -rf .tox
	rm -rf .cache
	rm -rf .pytest_cache
	find . -type f -name "*.pyc" -delete
	rm -rf $(find . -type d -name __pycache__)
	rm .coverage
	rm .coverage.*

dockerclean:
	docker system prune -f
	docker system prune -f --volumes
