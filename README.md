# Пример Gitlab CI для тестового django приложения

## Назначение: Данный репозиторий демонстрирует работу Gitlab CI системы на примере сборки django-проекта


[Ссылка на файл gitlab-ci.yml](./gitlab-ci.yml)

### Описание структуры:

```
image: docker:latest

services:
  - docker:dind
```
Данные параметры указывают то, что исполнение заданий будет происходить в docker контейнере на докер-раннер
P.S. данный метод запуска считается не самым надежным из-за того, что Docker был разработан так, чтобы иметь эксклюзивный доступ к каталогу, который он использует для хранения (обычно /var/lib/docker). А при запуске dind, дочерние процессы используют этот каталог одновременно.

```
variables:
  DOCKER_DRIVER: overlay2
  CONTAINER_COMPOSE_IMAGE: djangoapp
  CONTAINER_TEST_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
  CONTAINER_RELEASE_IMAGE: $CI_REGISTRY_IMAGE:latest
```
Здесь мы объявляем наши переменные, которые далее будут использоваться в заданиях, также здесь используются предопределенные переменные (Переменные которые объявляет сам GitLab для того чтобы внутри gitlab-ci мы могли получить доступ к уникальной информации нашего репозитория)

```
.before_script_template: &project_up
  before_script:
    - docker info
    - apk update
    - apk upgrade
    - apk add curl jq python3 python3-dev build-base libffi-dev libressl-dev gettext
    - curl -O https://bootstrap.pypa.io/get-pip.py
    - python3 get-pip.py
    - pip install docker-compose
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker-compose up -d
```
Тут мы описываем шаблон который переиспользуется в стейджах, в этом шаблоне мы устанавливаем библиотеки необходимые для docker-compose и сам docker-compose, а так же авторизовываемся во встроенном в Gitlab docker-registry, в самом конце запускаем стэк контейнеров, настройки которого, по умолчанию описываются в файле ./docker-compose.yml 

```
stages:
  - build
  - migrate
  - test
  - release
  - deploy
```
Stages используется для определения этапов, содержащих группы заданий. Порядок элементов в stages определяет порядок выполнения заданий: Задания на одном и том же этапе выполняются параллельно, Задания на следующем этапе выполняются после успешного завершения заданий предыдущего этапа.

```
build:
  image: docker:latest
  stage: build
  <<: *project_up
  script:
    - docker build --pull -t $CONTAINER_TEST_IMAGE .
    - docker push $CONTAINER_TEST_IMAGE
```
Создаем докер-образ описанный в Dockerfile, внутри которого, приложение версии коммита на который запущен Gitlab

Ссылка на докеробраз

[Dockerfile](./Dockerfile)

```
migrate:
  image: docker:latest
  stage: migrate
  <<: *project_up
  script:
    - echo "make migrate"
```
Запускаем (эммуляцию/заглушку) миграции внутри контейнера с приложением, проверяем исправность миграций

```
unit:
  image: docker:latest
  stage: test
  <<: *project_up
  script:
    - make test
```
Запускаем тесты внутри контейнера с приложением, прогоняем юнит-тесты

```
safety:
  image: docker:latest
  stage: test
  <<: *project_up
  script:
    - make checksafety
  only:
    - dev

style:
  image: docker:latest
  stage: test
  <<: *project_up
  script:
    - make checkstyle
  only:
    - dev
```
Для ветки "dev" дополнительно запустятся проверка библиотеками safety (Проверяет библиотеки на наличие уязвимостей ) и prospector ( инструмент для анализа кода Python и вывода информации об ошибках, потенциальных проблемах, нарушениях соглашений и сложности)

```
release-image:
  image: docker:latest
  stage: release
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
  script:
    - docker pull $CONTAINER_TEST_IMAGE
    - docker tag $CONTAINER_TEST_IMAGE $CONTAINER_RELEASE_IMAGE
    - docker push $CONTAINER_RELEASE_IMAGE
  only:
    - main
```
Для ветки main. После того как наше приложение прошло базовые проверки встроенные в CI, мы можем зарелизить данную версию приложения, поэтому мы фиксируем образ контейнера с приложением и отправляем его в Gitlab docker-registry с пометкой latest

```
deploy-staging:
  stage: deploy
  before_script:
    - apk add --update openssh-client
    - mkdir -p ~/.ssh
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' > ~/.ssh/id_rsa
    - chmod 700 ~/.ssh
    - chmod 700 ~/.ssh/id_rsa
    - eval "$(ssh-agent -s)"
    - ssh-add ~/.ssh/id_rsa
    - ssh-keyscan -H 'gitlab.com' >> ~/.ssh/known_hosts
    - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
  environment:
    name: staging
    url: https://staging.example.com
  script:
    - echo "deploy to server"
  only:
    - main
```
Для ветки main.Добавляем ssh ключ в окружение из-под которого будут отправляться команды на наш staging сервер, эмулируем деплой

```
deploy-prod:
  stage: deploy
  environment:
    name: production
    url: https://example.com
  before_script:
    - apk add --update openssh-client
    - mkdir -p ~/.ssh
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' > ~/.ssh/id_rsa
    - chmod 700 ~/.ssh
    - chmod 700 ~/.ssh/id_rsa
    - eval "$(ssh-agent -s)"
    - ssh-add ~/.ssh/id_rsa
    - ssh-keyscan -H 'gitlab.com' >> ~/.ssh/known_hosts
    - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
  script:
    - echo "deploy to server"
  when: manual
  only:
    - main
```
Для ветки main.Это задание можно запустить только в ручную в связи с тем,что выставлен параметр "when: manual" для того, чтобы дополнительно обезопасить себя от ошибок в деплоее на прод

## Схема:

![Схема](./img/scheme.png)